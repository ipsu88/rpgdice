﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;


namespace RPGDice
{
    [Activity(Label = "RPGDice", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            
            SetContentView(Resource.Layout.Main);

            TextView text = FindViewById<TextView>(Resource.Id.textView);
            Button button = FindViewById<Button>(Resource.Id.MyButton);
            button.Click += delegate { text.Text = printNbr(throwDice(1)); };

            TextView text1 = FindViewById<TextView>(Resource.Id.textView1);
            Button button1 = FindViewById<Button>(Resource.Id.MyButton1);
            button1.Click += delegate { text1.Text = printNbr(throwDice(2)); };

            TextView text2 = FindViewById<TextView>(Resource.Id.textView2);
            Button button2 = FindViewById<Button>(Resource.Id.MyButton2);
            button2.Click += delegate { text2.Text = printNbr(throwDice(3)); };

            TextView text3 = FindViewById<TextView>(Resource.Id.textView3);
            Button button3 = FindViewById<Button>(Resource.Id.MyButton3);
            button3.Click += delegate { text3.Text = printNbr(throwDice(4)); };

            TextView text4 = FindViewById<TextView>(Resource.Id.textView4);
            Button button4 = FindViewById<Button>(Resource.Id.MyButton4);
            button4.Click += delegate { text4.Text = printNbr(throwDice(5)); };

        }

        private static String printNbr(int dice)
        {

            Random r = new Random();
            return r.Next(1, dice).ToString();

        }

        private static int throwDice(int caseSwitch)
        {


            switch (caseSwitch)
            {
                case 1:
                    return 21;

                case 2:
                    return 7;

                case 3:
                    return 5;

                case 4:
                    return 9;

                default:
                    return 13;
            }
        }
    }
}

